# renovate-runner

Renovate Bot running in gitlab scheduled pipeline. Opens MRs to [eshop](https://gitlab.com/btravers/eshop).

See https://gitlab.com/renovate-bot/renovate-runner.
